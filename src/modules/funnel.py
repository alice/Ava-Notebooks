# pipery, a minimal functional pipelining framework. 


from functools import reduce


# Apply a function to each value of an iterable.

def Map(function):
    
    def ret(iterable):
        
        for value in iterable:     
            yield function(value)
    
    return ret


# Remove falsy values from an iterable according to a predicate.

def Filter(predicate):
    
    def ret(iterable):
        for value in iterable:
            if predicate(value): yield value
    
    return ret


# Perform a reduction on an iterable using a given function.

def Reduce(function):
    
    def ret(iterable):
        result = next(iterable)
        
        for value in iterable:
            result = function(result, value)
        
        yield result
    
    return ret


# The spread operator but for iterators.

def Spread(iterator):
    
    def ret(iterable):
        
        for value in iterable:
            for nested in iterator(value):
                yield nested
    
    return ret


# Group the values of an iterable into a single collection.

def Collect(iterable):
    yield list(iterable)


# Simplify the creation of iterator pipelines.

def Sequence(*iterators):
    
    def ret(iterable):
        return reduce(lambda a, b: b(a), iterators, iterable)
    
    return ret
        

# Simplify the creation of class-based pipeline stages.

class Function:
    
    def __call__(self, iterable):
        return self.process(iterable)
    

# Simplify the transport of complex data.

class Data:
    
    def __init__(self, **kwargs):
        self.__dict__ = kwargs
    
    
    def __repr__(self):
        return str(self.__dict__)