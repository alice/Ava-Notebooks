from funnel import Sequence
from searching import searching
from downloading import downloading
from preprocessing import preprocessing
from parsing import parsing
from postprocessing import postprocessing


symptoms_corpus = Sequence(
    searching,
    downloading,
    preprocessing,
    parsing,
    postprocessing
)