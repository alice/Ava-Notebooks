from pyquery import PyQuery
from funnel import Map
from funnel import Data

            
@Map
def parsing(markup):
    
    q = PyQuery(markup)
    name = q('h1').text()
    content = q('article').find('section p,section ul').text()
    
    return [name, content]