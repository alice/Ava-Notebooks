from funnel import Map


@Map
def postprocessing(parsed):
    
    name, content = parsed
    
    content = content.replace('\xa0', ' ')
    content = content.replace('\n', ' ')
    
    content = content.replace(' , ', ', ')
    content = content.replace(' . ', '. ')
    content = content.replace(' ; ', '; ')
    content = content.replace('.;', '.')
    
    content = content.replace('[paragraph]', '\n\n')
    content = content.encode('ascii', 'ignore')
    content = content.decode('utf8')
    
    return name, content