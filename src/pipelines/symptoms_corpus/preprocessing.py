from funnel import Map


@Map
def preprocessing(markup):
    
    markup = markup.replace('</p>', '[paragraph]</p>')
    markup = markup.replace(':[paragraph]', ':')
    markup = markup.replace('</ul>', '[paragraph]</ul>')
    markup = markup.replace('</figcaption>', '[paragraph]</p>')
    markup = markup.replace('<figcaption', '<p')
    markup = markup.replace('</li>', '; </li>')
    
    return markup