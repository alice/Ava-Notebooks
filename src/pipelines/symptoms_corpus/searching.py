from re import search
from funnel import Spread
from download import download


search_pattern =  r'(https://www.nhs.uk/conditions/(\w|-)+/(symptoms/)?)</loc>'


def search_iteratively(pattern, string):
    
    while 1:
        match = search(pattern, string)
        
        if not match:
            return
        
        result = match.group(1)
        cursor = match.end()
        string = string[cursor :]
        
        yield result
    

@Spread
def searching(url):
    
    return search_iteratively(search_pattern, download(url))
    